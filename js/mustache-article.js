/**
 * Created by Eduardo on 17/9/16.
 */


Promise.all([
    getMustacheTemplate('templates/header.html', 'json/header.json'),
    getMustacheTemplate('templates/articles/native/cover-picture.html', 'json/articles/native/article.json'),
    getMustacheTemplate('templates/articles/article-header.html', 'json/articles/native/article.json'),
    getMustacheTemplate('templates/articles/native/streamfields.html', 'json/articles/native/article.json'),
    getMustacheTemplate('templates/articles/related-news.html', 'json/articles/related-news.json'),
    getMustacheTemplate('templates/articles/latest-news.html', 'json/articles/latest-news.json'),
    getMustacheTemplate('templates/footer.html', 'json/footer.json')
]).then(function (responses) {
    'use strict';

    var index = 0,
        length = responses.length;

    for (index; index < length; index++) {
        document.body.appendChild(stringToHTML(responses[index]));
    }

    css();
    deferTag('img', 'data-src');
    deferTag('iframe', 'data-src');
    menuButtonLoadedHandler();
    navigationMenuLoadedHandler();
});