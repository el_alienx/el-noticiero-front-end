/**
 * Created by Eduardo on 17/1/16.
 */

function css() {
    'use strict';

    var l = document.createElement('link');
    l.rel = 'stylesheet';
    l.href = 'css/style.css';
    document.getElementsByTagName('head')[0].appendChild(l);
}

function backgroundImage() {
    'use strict';

    var backgroundImageDefer = document.getElementsByClassName('background-image'),
        index = 0,
        length = backgroundImageDefer.length,
        dataSource;

    for (index; index < length; index++) {
        if (backgroundImageDefer[index].getAttribute('data-src')) {
            dataSource = backgroundImageDefer[index].getAttribute('data-src');

            backgroundImageDefer[index].style.backgroundImage = 'url("' + dataSource + '")';
        }
    }
}

function deferTag(tag, attributeSrc) {
    'use strict';

    var tagDefer = document.getElementsByTagName(tag),
        index = 0,
        length = tagDefer.length,
        dataSource;

    for (index; index < length; index++) {
        if (tagDefer[index].getAttribute(attributeSrc)) {
            dataSource = tagDefer[index].getAttribute(attributeSrc);

            tagDefer[index].setAttribute('src', dataSource);
        }
    }
}