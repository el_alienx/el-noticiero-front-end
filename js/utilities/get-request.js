function getRequest(url) {
    'use strict';

    // Return a new promise.
    return new Promise(function (resolve, reject) {
        // Do the usual XHR stuff
        var request = new XMLHttpRequest();
        request.open('GET', url);

        request.onload = function () {
            // This is called even on 404 etc
            // so check the status
            if (request.status >= 200 && request.status < 400) {
                // Resolve the promise with the response text
                resolve(request.response);
            }
            else {
                // Otherwise reject with the status text
                // which will hopefully be a meaningful error
                reject(Error(request.statusText));
            }
        };

        // Handle network errors
        request.onerror = function () {
            reject(Error("Network Error"));
        };

        // Make the request
        request.send();
    });
}