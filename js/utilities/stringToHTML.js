/**
 * Created by Eduardo on 15/10/16.
 */


function stringToHTML(string) {
    'use strict';

    var temp = document.createElement('div');
    temp.innerHTML = string;

    return temp.firstChild;
}