function getMustacheTemplate(html, json) {
    'use strict';

    // Return a new promise. (resolve is the good return, reject the bad one)
    return new Promise(function (resolve) {
        // Load require files
        var myHTML;
        var myJSON;

        Promise.all([
            getRequest(html),
            getRequest(json)
        ]).then(function (responses) {
            myHTML = responses[0];
            myJSON = JSON.parse(responses[1]);

            resolve(Mustache.render(myHTML, myJSON));
        });
    });
}