/**
 * Created by Eduardo on 17/9/16.
 */

Promise.all([
    getMustacheTemplate('templates/header.html', 'json/header.json'),
    getMustacheTemplate('templates/video-library/video-player.html', 'json/video-library/videos.json'),
    getMustacheTemplate('templates/video-library/video-buttons.html', 'json/video-library/videos.json'),
    getMustacheTemplate('templates/subscribe.html', 'json/subscribe.json'),
    getMustacheTemplate('templates/footer.html', 'json/footer.json')
]).then(function (responses) {
    'use strict';

    var index = 0,
        length = responses.length;

    for (index; index < length; index++) {
        document.body.appendChild(stringToHTML(responses[index]));
    }

    css();
    deferTag('img', 'data-src');
    deferTag('iframe', 'data-src');
    menuButtonLoadedHandler();
    navigationMenuLoadedHandler();
});