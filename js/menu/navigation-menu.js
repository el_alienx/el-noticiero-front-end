/**
 * Created by Eduardo on 24/9/16.
 */


// HTML elements
var nav;
var buttonSubMenu;

var horizontalContainer;
var horizontalList;

var verticalContainer;
var verticalList;

// Measures
var mediaQuery = 650;
var headerWidth = 0;
var horizontalItemsWidth = 0;
var margin = 0; // include the right margin of last item and the submenu button


function navigationMenuLoadedHandler() {
    'use strict';

    nav = document.getElementById('nav');
    buttonSubMenu = document.getElementById('button-sub-menu');

    horizontalContainer = nav.getElementsByClassName('horizontal-container')[0];
    horizontalList = horizontalContainer.getElementsByClassName('menu-items')[0];

    verticalContainer = nav.getElementsByClassName('vertical-container')[0];
    verticalList = verticalContainer.getElementsByClassName('menu-items')[0];

    margin = getElementWidth(buttonSubMenu) + 50;

    window.addEventListener('resize', checkScreenWidth);

    checkScreenWidth();
}

function checkScreenWidth() {
    'use strict';

    // If mobile add all items to vertical list. Else add one by one to horizontal list.
    if (window.innerWidth < mediaQuery) {
        exchangeList(horizontalList, verticalList);
    }
    else {
        addHorizontalItem();
    }

    toggleDisplay();
}

function exchangeList(oldList, newList) {
    'use strict';

    var item,
        location;

    while (oldList.children.length > 0) {
        item = oldList.lastElementChild;
        location = newList.firstElementChild;

        newList.insertBefore(item, location);
    }
}

function addHorizontalItem() {
    'use strict';

    var item,
        location;

    updateWidths();

    while (headerWidth > horizontalItemsWidth && verticalList.children.length > 0) {
        horizontalList.appendChild(verticalList.firstElementChild);

        updateWidths();
    }

    // check if the while to an extra item
    if (horizontalItemsWidth >= headerWidth) {
        item = horizontalList.lastElementChild;
        location = verticalList.firstElementChild;

        verticalList.insertBefore(item, location);
    }
}

function toggleDisplay() {
    if (horizontalList.children.length === 0) {
        horizontalContainer.style.display = 'none';
    }
    else {
        horizontalContainer.style.display = 'block';
    }

    if (verticalList.children.length === 0) {
        verticalContainer.style.display = 'none';
        buttonSubMenu.style.display = 'none';
    }
    else {
        buttonSubMenu.style.display = 'inline-block';
    }
}

// Helper functions
function updateWidths() {
    'use strict';

    headerWidth = getElementWidth(nav);
    horizontalItemsWidth = getItemsWidth(horizontalList) + margin;
}

function getElementWidth(el) {
    'use strict';

    var width = el.offsetWidth,
        style = getComputedStyle(el);

    width += parseInt(style.marginLeft) + parseInt(style.marginRight);

    return width;
}

function getItemsWidth(el) {
    'use strict';

    var items = el.children,
        width = 0,
        index = 0,
        length = items.length;

    for (index; index < length; index++) {
        width += getElementWidth(items[index]);
    }

    return width;
}