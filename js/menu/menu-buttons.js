/**
 * Created by Eduardo on 17/1/16.
 */

var nav;
var buttonMenu;
var buttonSubMenu;
var verticalContainer;

function menuButtonLoadedHandler() {
    'use strict';

    nav = document.getElementById('nav');
    buttonMenu = document.getElementById('button-menu');
    buttonSubMenu = document.getElementById('button-sub-menu');


    verticalContainer = nav.getElementsByClassName('vertical-container')[0];

    buttonMenu.addEventListener('click', menuMobileToggleHandler);
    buttonSubMenu.addEventListener('click', menuTabletToggleHandler);
}

function menuMobileToggleHandler() {
    'use strict';

    if (verticalContainer.style.display === 'block') {
        verticalContainer.style.display = 'none';

        buttonMenu.getElementsByTagName('span')[0].classList.add('icon-menu');
        buttonMenu.getElementsByTagName('span')[0].classList.remove('icon-close');
    } else {
        verticalContainer.style.display = 'block';

        buttonMenu.getElementsByTagName('span')[0].classList.add('icon-close');
        buttonMenu.getElementsByTagName('span')[0].classList.remove('icon-menu');
    }
}

function menuTabletToggleHandler() {
    'use strict';

    verticalContainer.style.display =
        (verticalContainer.style.display !== 'block' ? 'block' : 'none');
}