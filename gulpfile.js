/**
 * Created by Eduardo on 1/10/16.
 */


var gulp = require('gulp');
var babel = require("gulp-babel");

//script paths
gulp.task("babel", function () {
  return gulp.src("source/js/elnoticiero.js")
    .pipe(babel())
    .pipe(gulp.dest("static/js/"));
});